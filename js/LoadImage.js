function LoadImage (src, loadCallback) {
    var image = new Image
    image.src = src
    image.onload = function () {
        loadCallback(image)
    }
}
