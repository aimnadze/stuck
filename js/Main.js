(function () {

    var Tile_Width = 10,
        Tile_Height = 10

    var width = 120,
        height = 120

    var scale

    var person_x = 0
    var pressed = Pressed()

    LoadImage('sprite.png', function (image) {

        function drawTile (tx, ty, x, y) {
            context.save()
            context.scale(scale, scale)
            context.drawImage(tiles[ty][tx], x, y)
            context.restore()
        }

        function drawTree (x) {
            drawTile(0, 3, x, -Tile_Height)
            for (var y = -Tile_Height * 2; y > -canvas.height; y -= Tile_Height) {
                drawTile(1, 3, x, y)
            }
        }

        function paint () {

            drawTile(moveColumns[column], row, -Tile_Width * 0.5 + person_x, -Tile_Height)

            var blocks = Math.ceil(canvas.width / (Tile_Width * scale))
            for (var x = -(Math.floor(blocks * 0.5) * Tile_Width + Tile_Width * 0.5); x < canvas.width; x += Tile_Width) {
                if (x < Tile_Width * -4.5 ||
                    x > Tile_Width * 0.5) drawTile(3, 2, x, -Tile_Width)
                drawTile(0, 2, x, 0)
                for (var y = Tile_Height; y < canvas.height; y += Tile_Height) {
                    drawTile(1, 2, x, y)
                }
            }

            drawTile(2, 2, Tile_Width * 13.5, -Tile_Height)
            drawTile(2, 2, Tile_Width * 8.5, -Tile_Height)
            drawTile(2, 2, Tile_Width * 2.5, -Tile_Height)
            drawTile(2, 2, Tile_Width * -3.5, -Tile_Height)
            drawTile(2, 2, Tile_Width * -11.5, -Tile_Height)

            drawTile(2, 3, Tile_Width * -15.5, -Tile_Height)
            drawTile(2, 3, Tile_Width * -12.5, -Tile_Height)
            drawTile(2, 3, Tile_Width * -8.5, -Tile_Height)
            drawTile(2, 3, Tile_Width * -5.5, -Tile_Height)
            drawTile(2, 3, Tile_Width * -1.5, -Tile_Height)
            drawTile(2, 3, Tile_Width * 5.5, -Tile_Height)
            drawTile(2, 3, Tile_Width * 12.5, -Tile_Height)

            drawTree(Tile_Width * -16.5)
            drawTree(Tile_Width * -14.5)
            drawTree(Tile_Width * -6.5)
            drawTree(Tile_Width * 4.5)
            drawTree(Tile_Width * 11.5)
            drawTree(Tile_Width * 14.5)

            drawTile(0, 4, Tile_Width * -2, Tile_Height * -3)
            drawTile(1, 4, -Tile_Width, Tile_Height * -3)
            drawTile(2, 4, 0, Tile_Height * -3)
            drawTile(3, 4, Tile_Width, Tile_Height * -3)

        }

        function resize () {

            canvas.width = innerWidth
            canvas.height = innerHeight

            var scaleWidth = Math.floor(canvas.width / width)
            var scaleHeight = Math.floor(canvas.height / height)
            scale = Math.min(scaleWidth, scaleHeight) * 2

            context.imageSmoothingEnabled = false
            context.translate(Math.round(canvas.width * 0.5), Math.round(canvas.height * 0.5 + Tile_Height * scale * 0.5))

            paint()

        }

        var moveColumns = [0, 1, 0, 2]

        var row = 0,
            column = 0

        var tiles = Tiles(image, Tile_Width, Tile_Height)

        var canvas = document.createElement('canvas')

        var context = canvas.getContext('2d')

        document.body.appendChild(canvas)
        addEventListener('resize', resize)
        resize()

        setInterval(function () {
            if (pressed.ArrowRight) {
                row = 0
                column = (column + 1) % moveColumns.length
                person_x += 2
                if (person_x > 20) person_x = 20
            } else if (pressed.ArrowLeft) {
                row = 1
                column = (column + 1) % moveColumns.length
                person_x -= 2
                if (person_x < -20) person_x = -20
            } else {
                column = 0
            }
            resize()
        }, 80)

    })

})()
