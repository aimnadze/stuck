function Pressed () {
    var pressed = Object.create(null)
    addEventListener('keyup', function (e) {
        delete pressed[e.key]
    })
    addEventListener('keydown', function (e) {
        pressed[e.key] = true
    })
    return pressed
}
