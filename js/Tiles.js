function Tiles (image, width, height) {
    var tiles = []
    for (var y = 0; y < image.height; y += height) {
        var row = []
        for (var x = 0; x < image.width; x += width) {

            var tile = document.createElement('canvas')
            tile.width = width
            tile.height = height

            tile.getContext('2d').drawImage(image, -x, -y)
            row.push(tile)

        }
        tiles.push(row)
    }
    return tiles
}
